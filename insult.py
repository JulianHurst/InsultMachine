#!/usr/bin/env python3

import random

def readnblines(path):
    i = 0
    with open(path, 'r') as f:
        while f.readline() != "":
            i = i + 1
    return i - 1

def insultpart(path, a):
    with open(path, 'r') as  f:
        for i in range(0, a):
            f.readline()
        aline = f.readline().splitlines()
    return ''.join(aline)

def min3(a, b, c):
    if a <= b and a <= c:
        return a
    if b <= a and b <= c:
        return b
    else:
        return c


a = readnblines('ainsult')
b = readnblines('binsult')
c = readnblines('cinsult')
n = min3(a, b, c)
a = random.randint(0, n)
b = random.randint(0, n)
c = random.randint(0, n)
aline = insultpart('ainsult', a)
bline = insultpart('binsult', b)
cline = insultpart('cinsult', c)

print(aline, bline, cline, sep=' ')

